import socketio
import eventlet
import os
import time
import socket
import cv2
import base64
import threading
import _thread


recv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
recv_socket.bind(("0.0.0.0", 9000))


sio = socketio.Server(cors_allowed_origins='*')
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'index.html'}
})

camera = cv2.VideoCapture('traffic.mp4')


def yababa():
    while True:
        grabbed, tmp_frame = camera.read()
        # tmp_frame = recv_socket.recv()
        encoded, buffer = cv2.imencode('.jpg', tmp_frame)
        # jpg_as_text = base64.b64encode(buffer).decode('utf-8')
        # sender_sock.send(jpg_as_text)d
        sio.emit('new_frame', buffer.tostring())


@sio.event
def connect(sid, environ):
    _thread.start_new_thread(yababa, ())
    print('connect ', sid)


@sio.event
def disconnect(sid):
    print('disconnect ', sid)


if __name__ == '__main__':
    eventlet.wsgi.server(eventlet.listen(('', 9001)), app)

    # while True:
    #     tmp_frame = recv_socket.recv()
    #     encoded, buffer = cv2.imencode('.jpg', tmp_frame)
    #     jpg_as_text = base64.b64encode(buffer)
    #     # sender_sock.send(jpg_as_text)d
    #     sio.emit('new_frame', jpg_as_text)
