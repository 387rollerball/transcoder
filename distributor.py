import cms
import utils
import queue
import socket
import select
import threading
import traceback

# CONSTS
CAMERA_FPS = 24

# SOCKET CONSTS
SOCKET_SELECT_TIMEOUT = 0.1


class Distributor:
    def __init__(self, id, address, sio):
        self.id = id
        self.address = address
        self.sio = sio
        self.consumers = []

        self.active = True

        self.frame_index = 0
        self.start_time = 0

        self.locker = threading.Lock()

        self.frame_flag = threading.Condition()
        self.received_frames = queue.Queue()

        self.connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connection.connect(address)

        self.network_thread = threading.Thread(target=self.network_handler)
        self.network_thread.start()

        self.recorder_thread = threading.Thread(target=self.recorder_handler)
        self.recorder_thread.start()

    def add_consumer(self, id):
        with self.locker:
            self.consumers.append(id)

    def remove_consumer(self, id):
        with self.locker:
            self.consumers.remove(id)

    def close(self):
        with self.locker:
            self.active = False

        with self.frame_flag:
            self.frame_flag.notify()

    def handle_frame_packet(self):
        try:
            if self.frame_index % CAMERA_FPS == 0:
                self.start_time = utils.time_ms()

            timestamp, size, frame = cms.receive_frame_packet(self.connection)

            self.received_frames.put(frame)

            # notify recorder new frame ready
            with self.frame_flag:
                self.frame_flag.notify()

            self.frame_index += 1

            # echo out each video's second
            if self.frame_index % CAMERA_FPS == 0:
                diff_time = utils.time_ms() - self.start_time
                sec_time = int(self.frame_index / CAMERA_FPS)
                print(" >> client {}: received sec {} (time={})".format(
                    self.address, sec_time, diff_time))
        except Exception as ex:
            traceback.print_exc()
            raise Exception(
                "error during handling frame packet from {}".format(self.address))

    def handle_get_camera_ack_packet(self):
        mode = cms.receive_get_camera_ack_packet(self.connection)

        print(" >> client {}: received get camera ack packet (mode={})".format(
            self.address, mode))

    def network_handler(self):
        try:
            print(" >> client {}: connected".format(self.address))

            cms.send_get_camera_packet(self.connection, 1)

            print(" >> client {}: sent get camera packet".format(self.address))

            while True:
                # sample client socket status
                readable, writable, exceptional = select.select(
                    [self.connection], [], [self.connection], SOCKET_SELECT_TIMEOUT)

                # check for receiver active status
                with self.locker:
                    if not self.active:
                        break

                # check for client socket error
                if exceptional:
                    raise Exception(
                        "error occured during waiting for a new data from {}".format(self.address))

                # check for new incoming data available
                if readable:
                    packet_type = cms.receive_packet_type(self.connection)

                    if packet_type == cms.GET_CAMERA_ACK_PACKET_TYPE_ID:
                        self.handle_get_camera_ack_packet()
                    elif packet_type == cms.FRAME_PACKET_TYPE_ID:
                        self.handle_frame_packet()
                    else:
                        raise Exception(
                            "received unsupported packet type {} from {}".format(packet_type, self.address))
        except Exception as ex:
            traceback.print_exc()
            print(" >> client exception {}: {}".format(self.address, ex))
        finally:
            try:
                self.connection.close()
            except Exception:
                pass
            print(" >> client {}: terminated".format(self.address))

    def recorder_handler(self):
        index = 0

        try:
            while True:
                # check for receiver active status
                with self.locker:
                    if not self.active:
                        break

                # wait for new frame signal
                with self.frame_flag:
                    self.frame_flag.wait()

                # check for receiver active status
                with self.locker:
                    if not self.active:
                        break

                # flush ready frames to video
                while not self.received_frames.empty():
                    if index % CAMERA_FPS == 0:
                        start_time = utils.time_ms()

                    # load frame and fix colors
                    frame = self.received_frames.get()
                    frame = numpy.frombuffer(frame, dtype=numpy.uint8)
                    frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
                    frame = cv2.resize(frame, (640, 368))
                    frame = cv2.imencode('.jpg', frame)[1].tobytes()

                    self.sio.emit('new_frame', frame,
                                  room=str(self.id))

                    index += 1

                    # echo out each video's second
                    if index % CAMERA_FPS == 0:
                        diff_time = utils.time_ms() - start_time
                        sec_time = int(index / CAMERA_FPS)
                        print(" >> recorder {}: save sec {} (time={})".format(self.address,
                                                                              sec_time, diff_time))
        except Exception as ex:
            print(" >> recorder exception {}: {}".format(self.address, ex))
        finally:
            print(" >> recorder {}: terminated".format(self.address))
