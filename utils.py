import time


def time_ms():
    return int(time.time() * 1000)
