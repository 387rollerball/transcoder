import cms
import cv2
import time
import utils
import queue
import struct
import socket
import argparse
import threading
import traceback

# CAMERA CONSTS
CAMERA_FPS = 24
CAMERA_INTERVAL = (1 / 24)  # ms
CAMERA_WIDTH = 640
CAMERA_HEIGHT = 368

# GLOBALS
g_debug = False
g_active = True

g_loaded_frames = queue.Queue()
g_ready_frames = queue.Queue()

g_locker = threading.Lock()
g_client_flag = threading.Condition()
g_camera_flag = threading.Condition()


def log_message(info):
    global g_debug

    if g_debug:
        print(info)


def loader_handler():
    global g_active
    global g_locker
    global g_loaded_frames

    # intialize locals
    index = 0

    try:
        while True:
            # check for active status
            with g_locker:
                if not g_active:
                    break

            # sample start time of each video's second
            if index % CAMERA_FPS == 0:
                start_time = utils.time_ms()

            # load new frame from disk
            frame = cv2.imread(r'frames/frame{}.jpg'.format(index))
            frame = cv2.resize(frame, (CAMERA_WIDTH, CAMERA_HEIGHT))
            # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YUV)
            frame = cv2.imencode('.jpg', frame)[1].tobytes()

            # put load frames on qeueu
            g_loaded_frames.put(frame)

            index += 1

            # echo out each video's second
            if index % CAMERA_FPS == 0:
                diff_time = utils.time_ms() - start_time
                sec_time = int(index / CAMERA_FPS)
                print(" >> loader: load sec {} (time={})".format(
                    sec_time, diff_time))
    except Exception as ex:
        log_message(" >> loader exception: {}".format(ex))
    finally:
        print(" >> loader: terminated")


def camera_handler():
    global g_active
    global g_locker
    global g_client_flag
    global g_camera_flag
    global g_ready_frames
    global g_loaded_frames

    # wait for client to be connected
    with g_client_flag:
        g_client_flag.wait()

    # check for streamer unactive status
    with g_locker:
        if not g_active:
            return

    index = 0
    start_time = 0
    delay_time = 0
    start_delay_time = utils.time_ms()

    print(" >> camera: captuering started ...")

    try:
        while True:
            # check for streamer unactive status
            with g_locker:
                if not g_active:
                    break

            # sample start time of each video's second
            if index % CAMERA_FPS == 0:
                start_time = utils.time_ms()

            delay_time = (utils.time_ms() - start_delay_time) / 1000

            # sleep for next fps interval
            if start_delay_time < CAMERA_INTERVAL:
                time.sleep(CAMERA_INTERVAL - delay_time)
            else:
                time.sleep(CAMERA_INTERVAL)

            start_delay_time = utils.time_ms()

            # fetch next frame from loaded frames
            g_ready_frames.put(g_loaded_frames.get())

            index += 1

            # notify for new frame
            with g_camera_flag:
                g_camera_flag.notify()

            # echo out each video's second
            if index % CAMERA_FPS == 0:
                diff_time = utils.time_ms() - start_time
                sec_time = int(index / CAMERA_FPS)
                print(" >> camera: captured sec {} (time={})".format(
                    sec_time, diff_time))
    except Exception as ex:
        log_message(" >> camera exception: {}".format(ex))
    finally:
        print(" >> camera: terminated")


def client_handler(ip, port):
    global g_active
    global g_locker
    global g_times
    global g_camera_flag
    global g_client_flag
    global g_ready_frames

    print(" >> client: connecting to remote receiver")

    try:
        # connect to remote receiver with TCP stream socket
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
            client.connect((ip, port))

            print(" >> client: connected successfully")

            cms.send_hello_packet(client, 1, CAMERA_FPS,
                                  (CAMERA_WIDTH, CAMERA_HEIGHT))

            print(" >> client: sent hello packet (id={}, fps={}, resolution={}x{})".format(
                1, CAMERA_FPS, CAMERA_WIDTH, CAMERA_HEIGHT))

            # receive hello ack packet
            packet_type = cms.receive_packet_type(client)
            if not packet_type == cms.HELLO_ACK_PACKET_TYPE_ID:
                raise Exception(
                    "error received invalid packet type {}".format(packet_type))

            print(" >> client: received hello ack packet")

            # notify camera to start captuering
            with g_client_flag:
                g_client_flag.notify()

            # intialize locals
            index = 0
            start_time = 0

            # serve forever
            while True:
                # check for active status
                with g_locker:
                    if not g_active:
                        break

                # wait for a new frame
                with g_camera_flag:
                    g_camera_flag.wait()

                # check for active status
                with g_locker:
                    if not g_active:
                        break

                # loop on each ready frames
                while not g_ready_frames.empty():
                    if index % CAMERA_FPS == 0:
                        start_time = utils.time_ms()

                    # fetch next frame
                    frame = g_ready_frames.get()
                    timestamp = utils.time_ms()

                    # send frame
                    cms.send_frame_packet(client, timestamp, frame)
                    # print(len(frame))

                    # print every sec sent
                    if (index + 1) % CAMERA_FPS == 0:
                        diff_time = utils.time_ms() - start_time
                        sec_time = int((index + 1) / CAMERA_FPS)
                        log_message(" >> client: sent {} sec (time={})".format(
                            sec_time, diff_time))

                    index += 1
    except Exception as ex:
        log_message(" >> client exception: {}".format(ex))
        traceback.print_exc()
    finally:
        # terminate streamer
        with g_locker:
            g_active = False
        print(" >> client: terminated")


def main():
    global g_debug
    global g_active
    global g_locker
    global g_camera_flag
    global g_client_flag

    # fetch arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("ip", help="receiver ip")
    parser.add_argument("port", help="receiver port", type=int)
    parser.add_argument('-d', '--debug', action='store_true',
                        help="shows debug prints")
    args = parser.parse_args()

    # save debug state
    g_debug = args.debug

    # print logo
    print("   _____  _        _____    _____  _                                             \r\n"
          "  / ____|| |      / ____|  / ____|| |                                            \r\n"
          " | |     | |     | (___   | (___  | |_  _ __  ___   __ _  _ __ ___    ___  _ __  \r\n"
          " | |     | |      \___ \   \___ \ | __|| '__|/ _ \ / _` || '_ ` _ \  / _ \| '__| \r\n"
          " | |____ | |____  ____) |  ____) || |_ | |  |  __/| (_| || | | | | ||  __/| |    \r\n"
          "  \_____||______||_____/  |_____/  \__||_|   \___| \__,_||_| |_| |_| \___||_|    \r\n")

    print(" >> connecting to {}:{}".format(args.ip, args.port))

    try:
        # start frames thread
        loader_thread = threading.Thread(target=loader_handler)
        loader_thread.start()

        # wait for loading a least 2 seconds
        time.sleep(2)

        # start camera thread
        camera_thread = threading.Thread(target=camera_handler)
        camera_thread.start()

        # start client thread
        client_thread = threading.Thread(
            target=client_handler, args=(args.ip, int(args.port)))
        client_thread.start()
    except KeyboardInterrupt:
        pass

    # loop untill keyboard interrupt
    try:
        while True:
            with g_locker:
                if not g_active:
                    break

            time.sleep(0.1)
    except KeyboardInterrupt:
        pass

    # shutdown server thread
    with g_locker:
        g_active = False

    with g_camera_flag:
        g_camera_flag.notify()

    with g_client_flag:
        g_client_flag.notify()

    # wait for threads to shutdown
    time.sleep(0.2)

    # bye bye
    print("")
    print(" >> shutdown")


if __name__ == "__main__":
    main()
