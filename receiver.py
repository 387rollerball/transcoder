import cms
import cv2
import time
import utils
import numpy
import queue
import socket
import struct
import select
import argparse
import threading
import socketio
import eventlet
import _thread
import traceback
import distributor
import requests
import json
eventlet.monkey_patch()

# SOCKET CONSTS
SOCKET_SELECT_TIMEOUT = 0.1

# CAMERA CONSTS
CAMERA_FPS = 24
CAMERA_WIDTH = 640
CAMERA_HEIGHT = 368

# GLOBALS
g_debug = False
g_address = ("", 0)
g_distributors = {}

sio = socketio.Server(cors_allowed_origins='*')
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'index.html'}
})


funny_names = ["Upper East Yokneam", "Netanya Pituach",
               "Kfar Saba Tahtit", "Rosh Haayin Temanim", "Ad Matai"]


@sio.event
def connect(sid, environ):
    print('connect ', sid)


@sio.event
def get_cams(sid):
    r = requests.get("http://" + g_address[0] + ":8080/API/AvailableCameras")
    j = json.loads(r.content)
    li = map(lambda x: {"id": x, "name": funny_names[x]})
    sio.emit('new_cams', [li])
    pass


@sio.event
def stop_stream(sid, cameraid):
    # TODO remove the user from the group
    pass


@sio.event
def start_stream(sid, cameraid):
    global g_distributors

    print(" >> start stream {}".format(cameraid))

    if cameraid not in g_distributors:
        print(" >> start dist {}".format(cameraid))
        g_distributors[cameraid] = distributor.Distributor(
            cameraid, g_address, sio)

    # g_distributors[cameraid].add_consumer(sid)
    sio.enter_room(sid, str(cameraid))


@sio.event
def disconnect(sid):
    print('disconnect ', sid)


def log_message(info):
    global g_debug

    if g_debug:
        print(info)


def main():
    global g_debug
    global g_distributors
    global g_address

    # fetch arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("ip", help="remote ip")
    parser.add_argument("port", help="remote port", type=int)
    parser.add_argument('-d', '--debug', action='store_true',
                        help="shows debug prints")
    args = parser.parse_args()

    # save debug state
    g_debug = args.debug

    # print logo
    print("   _____  _        _____   _____                   _                    \r\n"
          "  / ____|| |      / ____| |  __ \                 (_)                   \r\n"
          " | |     | |     | (___   | |__) | ___   ___  ___  _ __   __ ___  _ __  \r\n"
          " | |     | |      \___ \  |  _  / / _ \ / __|/ _ \| |\ \ / // _ \| '__| \r\n"
          " | |____ | |____  ____) | | | \ \|  __/| (__|  __/| | \ V /|  __/| |    \r\n"
          "  \_____||______||_____/  |_|  \_\\___| \___|\___||_|  \_/  \___||_|    \r\n")

    g_address = (args.ip, int(args.port))

    eventlet.wsgi.server(eventlet.listen(('', 9001)), app)

    # loop untill keyboard interrupt
    try:
        while True:
            pass
    except KeyboardInterrupt:
        pass

    # terminate distributors
    for distributor in g_distributors.values():
        distributor.close()

    # wait for threads to shutdown
    time.sleep(0.2)

    # bye bye
    print("")
    print(" >> shutdown")


if __name__ == '__main__':
    main()
