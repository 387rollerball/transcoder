import struct
import socket
import typing

# CMS HELLO PACKET CONSTS
# +---------------+---------+---------+----------+----------+
# | TYPE = (0x01) |    ID   |   FPS   |   WIDTH  |  HEIGHT  |
# +---------------+---------+---------+----------+----------+
# | 1 byte        |  1 byte |  1 byte |  2 bytes |  2 bytes |
# +---------------+---------+---------+----------+----------+
HELLO_PACKET_TYPE_ID = 0x01
HELLO_PACKET_TYPE_PARAM_LENGTH = 0x01
HELLO_PACKET_ID_PARAM_LENGTH = 0x01
HELLO_PACKET_FPS_PARAM_LENGTH = 0x01
HELLO_PACKET_WIDTH_PARAM_LENGTH = 0x02
HELLO_PACKET_HEIGHT_PARAM_LENGTH = 0x02

# CMS HELLO ACK PACKET CONSTS
# +---------------+---------+
# | TYPE = (0x02) |    ID   |
# +---------------+---------+
# | 1 byte        |  1 byte |
# +---------------+---------+
HELLO_ACK_PACKET_TYPE_ID = 0x02
HELLO_ACK_PACKET_TYPE_PARAM_LENGTH = 0x01
HELLO_ACK_PACKET_ID_PARAM_LENGTH = 0x01

# CMS FRAME PACKET CONSTS
# +---------------+------------+----------+---------------+
# | TYPE = (0x03) |  TIMESTAMP |   SIZE   |     FRAME     |
# +---------------+------------+----------+---------------+
# | 1 byte        |  8 bytes   |  4 bytes |  {SIZE} bytes |
# +---------------+------------+----------+---------------+
FRAME_PACKET_TYPE_ID = 0x03
FRAME_PACKET_TYPE_PARAM_LENGTH = 0x01
FRAME_PACKET_TIMESTAMP_PARAM_LENGTH = 0x08
FRAME_PACKET_SIZE_PARAM_LENGTH = 0x04

# CMS GET CAMERA PACKET CONSTS
# +---------------+---------+
# | TYPE = (0x04) |    ID   |
# +---------------+---------+
# | 1 byte        |  1 byte |
# +---------------+---------+
GET_CAMERA_PACKET_TYPE_ID = 0x04
GET_CAMERA_PACKET_TYPE_PARAM_LENGTH = 0x01
GET_CAMERA_PACKET_ID_PARAM_LENGTH = 0x01

# CMS GET CAMERA ACK PACKET CONSTS
# +---------------+---------+
# | TYPE = (0x05) |   MODE  |
# +---------------+---------+
# | 1 byte        |  1 byte |
# +---------------+---------+
GET_CAMERA_ACK_PACKET_TYPE_ID = 0x05
GET_CAMERA_ACK_PACKET_TYPE_PARAM_LENGTH = 0x01
GET_CAMERA_ACK_PACKET_MODE_PARAM_LENGTH = 0x01


def create_hello_packet(id, fps, resolution):
    type_bytes = struct.pack(">B", HELLO_PACKET_TYPE_ID)
    id_bytes = struct.pack(">B", id)
    fps_bytes = struct.pack(">B", fps)
    width_bytes = struct.pack(">H", resolution[0])
    height_bytes = struct.pack(">H", resolution[1])
    packet = type_bytes + id_bytes + fps_bytes + width_bytes + height_bytes

    return packet


def create_hello_ack_packet(id):
    type_bytes = struct.pack(">B", HELLO_ACK_PACKET_TYPE_ID)
    id_bytes = struct.pack(">B", id)
    packet = type_bytes + id_bytes

    return packet


def create_frame_packet(timestamp, frame):
    type_bytes = struct.pack(">B", FRAME_PACKET_TYPE_ID)
    timestamp_bytes = struct.pack(">Q", timestamp)
    size_bytes = struct.pack(">I", len(frame))
    packet = type_bytes + timestamp_bytes + size_bytes + frame

    return packet


def create_get_camera_packet(id):
    type_bytes = struct.pack(">B", GET_CAMERA_PACKET_TYPE_ID)
    id_bytes = struct.pack(">B", id)
    packet = type_bytes + id_bytes

    return packet


def create_get_camera_ack_packet(mode):
    type_bytes = struct.pack(">B", GET_CAMERA_ACK_PACKET_TYPE_ID)
    mode_bytes = struct.pack(">B", mode)
    packet = type_bytes + mode_bytes

    return packet


def receive_bytes(connection, amount):
    buffer = b''

    # loop until reaching desired bytes amount
    while len(buffer) < amount:
        remaining = amount - len(buffer)
        temp_buffer = connection.recv(remaining)

        # check for receiving error
        if not temp_buffer:
            raise Exception("error occured during receving {} bytes from {}".format(
                amount, connection.getpeername()))

        buffer += temp_buffer

    return buffer


def receive_packet_type(connection):
    try:
        # receive first packet byte as type param
        type_param = receive_bytes(connection, HELLO_PACKET_TYPE_PARAM_LENGTH)

        # convert big endian bytes string to unsigned char (1 byte)
        return struct.unpack(">B", type_param)[0]
    except Exception as ex:
        raise Exception("error occured during receving packet type param from {}".format(
            connection.getpeername()))


def receive_hello_packet(connection):
    try:
        # receive 6 bytes as hello packet
        id_param = receive_bytes(connection, HELLO_PACKET_ID_PARAM_LENGTH)
        fps_param = receive_bytes(connection, HELLO_PACKET_FPS_PARAM_LENGTH)
        width_param = receive_bytes(
            connection, HELLO_PACKET_WIDTH_PARAM_LENGTH)
        height_param = receive_bytes(
            connection, HELLO_PACKET_HEIGHT_PARAM_LENGTH)

        # convert big endian bytes string to unsigned char (1 byte)
        id_param = struct.unpack(">B", id_param)[0]

        # convert big endian bytes string to unsigned char (1 byte)
        fps_param = struct.unpack(">B", fps_param)[0]

        # convert big endian bytes string to unsigned short (2 bytes)
        width_param = struct.unpack(">H", width_param)[0]

        # convert big endian bytes string to unsigned short (2 bytes)
        height_param = struct.unpack(">H", height_param)[0]

        return id_param, fps_param, width_param, height_param
    except Exception as ex:
        raise Exception("error occured during receving hello packet from {}".format(
            connection.getpeername()))


def receive_hello_ack_packet(connection):
    try:
        # receive 1 byte as hello ack packet
        id_param = receive_bytes(connection, HELLO_ACK_PACKET_ID_PARAM_LENGTH)

        # convert big endian bytes string to unsigned char (1 byte)
        id_param = struct.unpack(">B", id_param)[0]

        return id_param
    except Exception as ex:
        raise Exception("error occured during receving hello ack packet from {}".format(
            connection.getpeername()))


def receive_frame_packet(connection):
    try:
        # receive 12 bytes as frame packet
        timestamp_param = receive_bytes(
            connection, FRAME_PACKET_TIMESTAMP_PARAM_LENGTH)
        size_param = receive_bytes(connection, FRAME_PACKET_SIZE_PARAM_LENGTH)

        # convert big endian bytes string to unsigned long (8 bytes)
        timestamp_param = struct.unpack(">Q", timestamp_param)[0]

        # convert big endian bytes string to unsigned int (4 bytes)
        size_param = struct.unpack(">I", size_param)[0]

        # receive {size_param} bytes as frame parameter
        frame_param = receive_bytes(connection, size_param)

        return timestamp_param, size_param, frame_param
    except Exception as ex:
        raise Exception("error occured during receving frame packet from {}".format(
            connection.getpeername()))


def receive_get_camera_packet(connection):
    try:
        # receive 1 byte as get camera packet
        id_param = receive_bytes(connection, GET_CAMERA_PACKET_ID_PARAM_LENGTH)

        # convert big endian bytes string to unsigned char (1 byte)
        id_param = struct.unpack(">B", id_param)[0]

        return mode_param
    except Exception as ex:
        raise Exception("error occured during receving get camera packet from {}".format(
            connection.getpeername()))


def receive_get_camera_ack_packet(connection):
    try:
        # receive 1 byte as get camera ack packet
        mode_param = receive_bytes(
            connection, GET_CAMERA_ACK_PACKET_MODE_PARAM_LENGTH)

        # convert big endian bytes string to unsigned char (1 byte)
        mode_param = struct.unpack(">B", mode_param)[0]

        return mode_param
    except Exception as ex:
        raise Exception("error occured during receving get camera ack packet from {}".format(
            connection.getpeername()))


def send_hello_packet(connection, id, fps, resolution):
    try:
        packet = create_hello_packet(id, fps, resolution)
        connection.send(packet)
    except Exception as ex:
        raise Exception("error occured during sending hello packet to {}".format(
            connection.getpeername()))


def send_hello_ack_packet(connection, id):
    try:
        packet = create_hello_ack_packet(id)
        connection.send(packet)
    except Exception as ex:
        raise Exception("error occured during sending hello ack packet to {}".format(
            connection.getpeername()))


def send_frame_packet(connection, timestamp, frame):
    try:
        packet = create_frame_packet(timestamp, frame)
        connection.send(packet)
    except Exception as ex:
        raise Exception("error occured during sending frame packet to {}".format(
            connection.getpeername()))


def send_get_camera_packet(connection, id):
    try:
        packet = create_get_camera_packet(id)
        connection.send(packet)
    except Exception as ex:
        raise Exception("error occured during sending frame packet to {}".format(
            connection.getpeername()))
